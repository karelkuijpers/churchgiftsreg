#
# Table structure for table 'bedieningsprofiel'
#
CREATE TABLE `bedieningsprofiel` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `verlangen` tinyblob,
  `gaven` tinyblob,
  `stijlenergie` enum('Taakgericht','Mensgericht','Beide') DEFAULT NULL,
  `stijlorganisatie` enum('Gestructureerd','Ongestructureerd','Beide') DEFAULT NULL,
  `leuk_in_werk` tinyblob,
  `ervaring` tinyblob,
  `inzet` tinyblob,
  `wat_aanspreekt` mediumblob,
  `anders` tinyblob,
  `vaardigheden` tinyblob,
  `overig` tinyblob,
  `voorgestelde_bedieningen` mediumtext,
  `datum_lst_gesprek` date NULL DEFAULT NULL,
  `adviseur_id` int(11) NOT NULL DEFAULT '0',
  `datum_lst_verzoekgesprek` date NULL DEFAULT NULL,
  `redenweigering` varchar(255) NOT NULL DEFAULT '',
  `person_id` tinyblob,
  `steller` int(11) NOT NULL DEFAULT '0',
  `datum_wijziging` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
  # ON update CURRENT_TIMESTAMP
  `deleted` smallint(5) unsigned NOT NULL DEFAULT '0',
  `hidden` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) DEFAULT CHARACTER SET utf8 ;

#
# Table structure for table 'gaventest'
#
CREATE TABLE `gaventest` (
  `uid` int(11) NOT NULL,
  `question` varchar(256) NOT NULL,
  `destination` tinyint(2) NOT NULL,
  PRIMARY KEY (`uid`)
) DEFAULT CHARACTER SET utf8;

#
# Table structure for table 'stijltest'
#
CREATE TABLE `stijltest` (
  `uid` int(11) NOT NULL,
  `question` varchar(60) NOT NULL,
  `stijlleft` varchar(60) NOT NULL,
  `stijlright` varchar(60) NOT NULL,
  `type` char(1) NOT NULL COMMENT 'O=organisatie,E=energie',
  PRIMARY KEY (`uid`)
) DEFAULT CHARACTER SET utf8;

