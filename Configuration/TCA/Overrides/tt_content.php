<?php
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3') or die();

/***************
 * Plugin
 */
 ExtensionManagementUtility::addTcaSelectItemGroup(
    'tt_content',
    'list_type',
    'churchgiftsreg',
    'Gavenprofielen'
);


ExtensionUtility::registerPlugin(
        'Churchgiftsreg',
        'BedieningsprofielSearch',
        'Gavenbank',
        'content-plugin-churchgiftsreg-bedieningsprofielsearch',
        'churchgiftsreg',
    );
ExtensionUtility::registerPlugin(
        'Churchgiftsreg',
        'BedieningsprofielDetail',
        'Gavenprofiel',
        'content-plugin-churchgiftsreg-bedieningsprofieldetail',
        'churchgiftsreg',
    );

/*$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['churchgiftsreg_bedieningsprofiel'] = 'recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['churchgiftsreg_bedieningsprofiel'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue('churchgiftsreg_bedieningsprofiel',
    'FILE:EXT:churchgiftsreg/Configuration/FlexForms/flexform_bedieningsprofiel.xml'); 

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToInsertRecords('bedieningsprofiel'); */
