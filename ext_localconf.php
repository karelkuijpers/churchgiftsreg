<?php
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;
use Parousia\Churchgiftsreg\Controller\BedieningsprofielController;
defined('TYPO3') || die('Access denied.');

ExtensionUtility::configurePlugin(
         'Churchgiftsreg',
         'BedieningsprofielSearch',
         [BedieningsprofielController::Class => 'searchForm,detail,selectedList,save,edit,delete,export,gaventest,stijltest'],
         // non-cacheable actions
		 [BedieningsprofielController::Class => 'searchForm,detail,selectedList,save,edit,delete,export,gaventest,stijltest']
);
ExtensionUtility::configurePlugin(
        'Churchgiftsreg',
        'BedieningsprofielDetail',
         [BedieningsprofielController::Class => 'detail,save,edit,gaventest,stijltest'],
         // non-cacheable actions
         [BedieningsprofielController::Class => 'detail,save,edit,gaventest,stijltest'],
);


