<?php
namespace Parousia\Churchgiftsreg\Controller;
use \Parousia\Churchlogin\Hooks\XLSXWriter;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use Parousia\Churchgiftsreg\Hooks\churchgiftsreg_div;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Http\ForwardResponse;


/***
 *
 * This file is part of the "Bedieningsprofielen" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * BedieningsprofielController
 */
class BedieningsprofielController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \Parousia\Churchgiftsreg\Domain\Repository\BedieningsprofielRepository
     */
    protected $BedieningsprofielRepository;
	public static $includedMyJs;
	var $pageId;
	var $args;
	var $search='';
	var $stack='';
	var $downloadfile='';
	var $userid;
	var $frontendUser;

    /**
     * Inject a bedieningsprofiel repository to enable DI
     *
     * @param \Parousia\Churchgiftsreg\Domain\Repository\BedieningsprofielRepository $bedieningsprofielRepository
     */
    public function injectBedieningsprofielRepository(\Parousia\Churchgiftsreg\Domain\Repository\BedieningsprofielRepository $bedieningsprofielRepository)
    {
        $this->BedieningsprofielRepository = $bedieningsprofielRepository;
    }

    /**
     * Initialize 
     */
    public function initializeAction(): void
    {
		$this->args=$this->request->getArguments();
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($this->args['downloadfile']))
		{
			$this->downloadfile=$this->args['downloadfile'];
		}
		if (is_array($this->request->getParsedBody()))
		{
			$ParsedBody=$this->request->getParsedBody();
	//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction ParsedBody: '.urldecode(http_build_query($ParsedBody,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			if (isset($ParsedBody['tx_churchgiftsreg_bedieningsprofiel']))$this->args['tx_churchgiftsreg_bedieningsprofiel']=$ParsedBody['tx_churchgiftsreg_bedieningsprofiel'];
		}

		if (isset($this->args['search']))
		{
			$this->search=$this->args['search'];
		} else $this->search=NULL;
	
		if (isset($this->args['stack']))
		{
			$this->stack=$this->args['stack'];
		} else $this->stack=NULL;
		if (!empty($this->stack))
		{
			$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		} else $this->stack=array();
		$pageArguments = $this->request->getAttribute('routing');
		$this->pageId = $pageArguments->getPageId();
		$this->frontendUser = $this->request->getAttribute('frontend.user');
		$this->userid = $this->frontendUser->user['person_id'];
	//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'initializeAction userid: '.$this->userid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');


	}
	
    /**
     * action searchForm
     *
     * @return void
     */
    public function searchFormAction(): ResponseInterface
    {
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'BedieningsprofielController searchFormAction'."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (!(int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen"))die("You don't have the privilege to perform this action");
		if (is_array($this->stack))
		{
			$key=array_search('selectedList', array_column($this->stack, 'action'));
			if ($key)$this->search=$this->stack[$key]["search"];
		}
		
        if (is_null($this->search)) 
		{
            $this->search = array();
			$this->search['islid']="";
			$this->search['gesprekgewenst']="0";
        }
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SearchForm search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		// reinitialize stack:
		$this->stack=array(array("action"=>"searchForm","search"=>$this->search));
//var_dump($this->search);
    	$assignedValues = [
			'search' => $this->search,
			'stack' => json_encode($this->stack),
			'gavenoptions'=> churchgiftsreg_div::$a_gaven,
			'vaardighedenoptions'=> churchgiftsreg_div::$a_vaardigheid,
			'bedieningsprofielreg' => (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens Schrijven"),
			'adviseursbeschikbaar' => '0',	
			'extPath' => PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchgiftsreg')),
        ];
        $this->view->assignMultiple($assignedValues); 
    	return $this->responseFactory
        ->createResponse()
        ->withHeader('Cache-Control', 'no-cache')
        ->withHeader('Content-Type', 'text/html; charset=utf-8')
        ->withStatus(200, 'Super ok!')
        ->withBody($this->streamFactory->createStream($this->view->render()));

		//$this->view->assign('search',$this->search);
    }

    /**
     * action selectedList
     *
     * @return void
     */
    public function selectedListAction(): ResponseInterface
    {
		if (!(int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen"))die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search : '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack : '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (!is_array($this->stack))$this->stack=[array('action'=>"searchForm","search"=>$this->search),array("action"=>"selectedList","search"=>$this->search)];
		else
		{
			$key=array_search('selectedList', array_column($this->stack, 'action'));
			if (!$key)$this->stack[]=array("action"=>"selectedList","search"=>$this->search);
			else 
			{
				$this->search=$this->stack[$key]['search'];
				array_splice($this->stack,$key+1);  // reset stack from this action off
			}
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (empty($this->search['general']) && empty($this->search['naw']) && empty($this->search['verlangen'])&&empty($this->search['gaven'])&&empty($this->search['vaardigheden'])&&empty($this->search['gesprekgewenst']))
			$bedieningsprofielen=array();
		else 
		{
			$bedieningsprofielen=$this->BedieningsprofielRepository->findSelection($this->search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList search: '.urldecode(http_build_query($bedieningsprofielen,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			foreach ($bedieningsprofielen as $key=>$bedieningsprofiel)
			{
				if (!empty($bedieningsprofiel["gaven"])){$aGaven=explode(",",trim($bedieningsprofiel["gaven"]));}else{$aGaven=array();}
				$gaven=array();
			 	for ($i=0;$i<count($aGaven);$i++)
				{	
					$gaven[$i]=churchgiftsreg_div::$a_gaven[$aGaven[$i]];
				}
				$bedieningsprofielen[$key]["gaven"]=implode(', ',$gaven);
			}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList bedieningsprofielen: '.urldecode(http_build_query($bedieningsprofielen,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		}
//		var_dump($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['churchadmin']);
    	$assignedValues = [
			'bedieningsprofielen' => $bedieningsprofielen,
			'downloadfile' => $this->downloadfile,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'bedieningsprofielreg' => (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens schrijven"),
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SelectedList assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
        $this->view->assignMultiple($assignedValues); 
    	return $this->responseFactory
        ->createResponse()
        ->withHeader('Cache-Control', 'no-cache')
        ->withHeader('Content-Type', 'text/html; charset=utf-8')
        ->withStatus(200, 'Super ok!')
        ->withBody($this->streamFactory->createStream($this->view->render()));
    }

    /**
     * action detail
     *
     * @return void
     */
    public function detailAction(): ResponseInterface
    { 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detail args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$ownprofile=0;
		if (is_array($this->args)&&count($this->args)>0)
		{
			if (isset($this->args['personid']))$personid=intval($this->args['personid']);else $personid=0;
			$this->stack=$this->args['stack'];
			$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		}
		else
		{
			$personid=$this->userid;
			$ownprofile=1;
			$this->stack=array();
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detail personid: '.$personid."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detailAction persid: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (isset($this->stack) && count($this->stack)>0)
		{
			$key=array_search('detail', array_column($this->stack, 'action'));
			if ($key===false )
			{
				// forward:
				$this->stack[]=array("action"=>"detail","controller"=>"Bedieningsprofiel","extensionName"=>"churchgiftsreg","pluginName"=>"Bedieningsprofiel","pageUid"=>$this->pageId,"personid"=>$personid,"ownprofile"=>$ownprofile);
				$return='';
			}
			else
			{
				$ownprofile=$this->stack[$key]['ownprofile'];
				$personid=$this->stack[$key]['personid'];
				array_splice($this->stack,$key+1);  // update: reset stack after this action off
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detail key: '.$key.'; stack na splice:'.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
/*				if (count($this->stack)>0)
					$return=$this->stack[$key-1]["action"]; 
				else */
				$return='';
			}
		}
		else 
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detail geen stack gevonden '."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$this->stack[]=array("action"=>"detail","controller"=>"Bedieningsprofiel","extensionName"=>"churchgiftsreg","pluginName"=>"Bedieningsprofiel","pageUid"=>$this->pageId,"personid"=>$personid,"ownprofile"=>$ownprofile);
			$return='';
		}
		if (!(int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen") && !$ownprofile )die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detail return: '.$return.'; stack::'.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');


	//	var_dump($bedieningsprofielid);
        $bedieningsprofiel = $this->BedieningsprofielRepository->findByUid($personid);
		if (!empty($bedieningsprofiel["gaven"])){$aGaven=explode(",",trim($bedieningsprofiel["gaven"]));}else{$aGaven=array();}
		if (!empty($bedieningsprofiel["vaardigheden"])){$aVaardigheden=explode(",",trim($bedieningsprofiel["vaardigheden"]));}else{$aVaardigheden=array();}
		if (!empty($bedieningsprofiel["wat_aanspreekt"])){$aWatAanspr=explode(",",$bedieningsprofiel["wat_aanspreekt"]);}else{$aWatAanspr=array();}
		$aGavenFile=array();
	 	for ($i=0;$i<count($aGaven);$i++)
		{	
			$aGaven[$i]=churchgiftsreg_div::$a_gaven[$aGaven[$i]];
			$filename="EXT:churchgiftsreg/Resources/Public/Documents/".str_replace(' ','_',$aGaven[$i]).'.htm';
			$aGavenFile[$i]=rtrim(GeneralUtility::getIndpEnv('parousiazoetermeer.nl'), '/')
            . PathUtility::getAbsoluteWebPath(GeneralUtility::getFileAbsFileName($filename));
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'detail aGavenFile: '.urldecode(http_build_query($aGavenFile,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		}
		
		$bedieningsprofiel["gaven"]=$aGaven;
		$bedieningsprofiel["gavenfiles"]=$aGavenFile;
		$bedieningsprofiel["stijlfile"]=rtrim(GeneralUtility::getIndpEnv('parousiazoetermeer.nl'), '/')
            . PathUtility::getAbsoluteWebPath(GeneralUtility::getFileAbsFileName("EXT:churchgiftsreg/Resources/Public/Documents/Wat_is_jouw_stijl.htm"));
		
		// voeg subarray's van $a_vaardigheid samen tot 1 grote array:
		$a_vaardigtabel=array();
		foreach (churchgiftsreg_div::$a_vaardigheid as $a_vaard){$a_vaardigtabel=$a_vaardigtabel+$a_vaard;}
		$vaardigheden='';
 		for ($i=0;$i<count($aVaardigheden);$i++)
		{	if ($i>0){$vaardigheden.= ", ";}
			$vaardigheden.= $a_vaardigtabel[$aVaardigheden[$i]];
		}
		if (!empty($bedieningsprofiel["overig"]))
		{
			if ($i>0){$vaardigheden.= ", ";}
			$vaardigheden.= $bedieningsprofiel["overig"];
		}
		$bedieningsprofiel["vaardigheden"]=$vaardigheden;
/*		$overig="";
		if (!empty($bedieningsprofiel['overig'])) {$overig="<tr><td class=\"labelchurchadmin\">Overig:</td><td>".$bedieningsprofiel['overig']."</td></tr>";} */
		$aanspreekt='';
 		for ($i=0;$i<count($aWatAanspr);$i++)
		{	if ($i>0){$aanspreekt.= ", ";}
			$aanspreekt.= churchgiftsreg_div::$a_Aanspreek[$aWatAanspr[$i]];
		}
		if (!empty($bedieningsprofiel["anders"]))
		{
			if ($i>0){$aanspreekt.= ", ";}
			$aanspreekt.= $bedieningsprofiel["anders"];
		}
		$bedieningsprofiel["wat_aanspreekt"]=$aanspreekt;


		$bedieningsprofielreg = (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens schrijven");
		$extensionConfiguration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('churchgiftsreg');

		if ($ownprofile)
		{
			//Mijn profiel:
			$assignedValues = [
			'personid' => $personid,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'return'=>'',
			'ownprofile' => $ownprofile,
			'adviseursbeschikbaar' => '0',	
			'pageuidperson' => $extensionConfiguration['pagenbrsingleperson']	
			];
		}
		else
			$assignedValues = [
			'personid' => $personid,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'ownprofile' => $ownprofile,
			'currentPid' => $this->pageId,
			'schrijvenbedieningsprofielen' => churchpersreg_div::Heeftpermissie("Netwerkgegevens schrijven"),
			'bedieningsprofielreg' => $bedieningsprofielreg,
			'return'=>'selectedList',		
			'adviseursbeschikbaar' => '0',
			'pageuidperson' => $extensionConfiguration['pagenbrsingleperson']	
        ];
		$assignedValues['extPath']= PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchgiftsreg'));
		$assignedValues['bedieningsprofiel']=$bedieningsprofiel;

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": detailAction assignedValues: ".\TYPO3\CMS\Core\Utility\GeneralUtility::array2xml($assignedValues)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
        $this->view->assignMultiple($assignedValues); 
    	return $this->responseFactory
        ->createResponse()
        ->withHeader('Cache-Control', 'no-cache')
        ->withHeader('Content-Type', 'text/html; charset=utf-8')
        ->withStatus(200, 'Super ok!')
        ->withBody($this->streamFactory->createStream($this->view->render()));

    } 

    /**
     * action edit
     *
     * @return void
     */
    public function editAction(): ResponseInterface
    {
		$bedieningsprofielreg= (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens schrijven");
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction userid:'.$this->userid.'; args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (empty($this->args['personid']))$personid=$this->userid; else $personid=$this->args['personid'];
		if (isset($this->args['ownprofile']))$ownprofile=$this->args['ownprofile']; else $ownprofile=0;
		if (isset($this->args['read']))$read=$this->args['read'];else $read='';
		if (isset($this->args['subaction']))$subaction=$this->args['subaction'];else $subaction='';
		//$subaction=$this->args['subaction'];
		if (!$bedieningsprofielreg && $personid!=$this->userid) die("You don't have the privilege to perform this action");
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//$return=$this->stack[count($this->stack)-1]['action'];
		$key=array_search('edit', array_column($this->stack, 'action'));
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction key: '.$key."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (!$key || !is_array($this->stack) || $read)
		{
			// forward:
			$this->stack[]=array("action"=>"edit","personid"=>$personid);
			$bedieningsprofiel = $this->BedieningsprofielRepository->findByUid($personid);
			if (!empty($bedieningsprofiel["gaven"])){$aGaven=explode(",",trim($bedieningsprofiel["gaven"]));}else{$aGaven=array();}
			$bedieningsprofiel["gaven"]=$aGaven;
			if (!empty($bedieningsprofiel["wat_aanspreekt"])){$aAanspreek=explode(",",trim($bedieningsprofiel["wat_aanspreekt"]));}else{$aAanspreek=array();}
			$bedieningsprofiel["wat_aanspreekt"]=$aAanspreek;
			if (!empty($bedieningsprofiel["vaardigheden"])){$aVaardigheden=explode(",",trim($bedieningsprofiel["vaardigheden"]));}else{$aVaardigheden=array();}
			$bedieningsprofiel["vaardigheden"]=$aVaardigheden;
		}
		else
		{
			// return to this action:
			$bedieningsprofiel = $this->stack[count($this->stack)-1]["bedieningsprofiel"];
			if ($subaction=='gavenberekenen')
			{
				$aGaven=$this->gavenberekenen($personid);
				$bedieningsprofiel["gaven"]=$aGaven;
			}
			if ($subaction=='stijlberekenen')
			{
				$aStijlen=$this->stijlberekenen($personid);
				$bedieningsprofiel['stijlorganisatie']=$aStijlen['stijlorganisatie'];
				$bedieningsprofiel['stijlenergie']=$aStijlen['stijlenergie'];
			}
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'editAction na key bedieningsprofiel: '.urldecode(http_build_query($bedieningsprofiel,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//			$bedieningsprofiel = json_decode($this->args['bedieningsprofiel'],true,20,JSON_INVALID_UTF8_IGNORE);
//			array_pop($this->stack);  // reset stack from this action off
			array_splice($this->stack,$key+1);  // update: reset stack after this action off
			//$return="detail";
		}
		$bedieningsprofiel["passionfile"]=rtrim(GeneralUtility::getIndpEnv('parousiazoetermeer.nl'), '/')
            . PathUtility::getAbsoluteWebPath(GeneralUtility::getFileAbsFileName("EXT:churchgiftsreg/Resources/Public/Documents/Analyse_verlangen.htm"));

		//if (empty($return))	$return = 'detail';
		$assignedValues = [
			'bedieningsprofiel' => $bedieningsprofiel,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'ownprofile' => $ownprofile,
			'adviseursbeschikbaar' =>'0',
			'gavenoptions'=> churchgiftsreg_div::$a_gaven,
			'aanspreekoptions'=> churchgiftsreg_div::$a_Aanspreek,
			'vaardighedenoptions'=> churchgiftsreg_div::$a_vaardigheid,
			'scrijvenbedieningsprofielen' => churchpersreg_div::Heeftpermissie("Netwerkgegevens schrijven"),
			'bedieningsprofielreg' => (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens schrijven"),
			'return' => "detail",
			'extPath'=> PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath('churchgiftsreg')),

        ];
        $this->view->assignMultiple($assignedValues); 
    	return $this->responseFactory
        ->createResponse()
        ->withHeader('Cache-Control', 'no-Cache')
        ->withHeader('Content-Type', 'text/html; charset=utf-8')
        ->withStatus(200, 'Super ok!')
        ->withBody($this->streamFactory->createStream($this->view->render()));

    } 
    /**
     * action gaventest
     *
     * @return void
     */
    public function gaventestAction(): ResponseInterface
    {
		$bedieningsprofielreg= (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens schrijven");
		if(isset($this->args['bedieningsprofiel']))$bedieningsprofiel=$this->args['bedieningsprofiel'];else $bedieningsprofiel='';
		//$bedieningsprofiel=$this->args["bedieningsprofiel"];
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'gaventestAction bedieningsprofiel: '.urldecode(http_build_query($bedieningsprofiel,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if(isset($bedieningsprofiel['person_uid']))$personid=$bedieningsprofiel['person_uid'];else $personid='';
		$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		$return=$this->stack[count($this->stack)-1]['action'];
		$this->stack[]=array("action"=>"gaventest","personid"=>$personid,"bedieningsprofiel"=>$bedieningsprofiel);
		$aGaventest=$this->BedieningsprofielRepository->findGaventest();
//		$this->stack[]=array("action"=>"gaventest","personid"=>$personid,"bedieningsprofiel"=>$bedieningsprofiel);
		$assignedValues = [
			'gaventest' => $aGaventest,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'return' => $return,
			'personid' => $personid,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'gaventestAction assignedValues: '.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
        $this->view->assignMultiple($assignedValues); 
    	return $this->responseFactory
        ->createResponse()
        ->withHeader('Cache-Control', 'no-cache')
        ->withHeader('Content-Type', 'text/html; charset=utf-8')
        ->withStatus(200, 'Super ok!')
        ->withBody($this->streamFactory->createStream($this->view->render()));
	}
	

    /**
     * function gavenberekenen
     *
     * @return $aGaven 
     */
    public function gavenberekenen($personid)
    {
	//	$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		//$personid=$this->args["personid"];
		// calculate gaventest score:
		$agavenscore=array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
		for ($i=1; $i<=133; $i++) 
		{	$score=$this->args['tx_churchgiftsreg_bedieningsprofiel']["a".$i];
		 	$dest= ($i-1) % 19;
			$agavenscore[$dest]+=$score;
		}
		$ascore=array_combine(array("d","a","q","e","n","c","g","i","j","f","h","r","l","m","b","p","k","o","s"),$agavenscore);
	//		$FoundPersons.="Agaven org:".TYPO3\CMS\Core\Utility\GeneralUtility::arrayToLogString($aGaven)."<br>agavenscore:".TYPO3\CMS\Core\Utility\GeneralUtility::arrayToLogString($agavenscore).'<br>ascore:'.TYPO3\CMS\Core\Utility\GeneralUtility::arrayToLogString($ascore).'<br>';
		arsort($ascore);
		while (array_search(0,$ascore)) array_pop($ascore);
		$aGaven=array_keys($ascore);
//		$FoundPersons.="ascore na sort:".TYPO3\CMS\Core\Utility\GeneralUtility::arrayToLogString($ascore).'<br>aGaven:'.TYPO3\CMS\Core\Utility\GeneralUtility::arrayToLogString($aGaven).'<br>';
		$aGaven=array_slice($aGaven,0,3);
		if (empty($aGaven) || count($aGaven)==0)$aGaven=["s","e","q"];
		return $aGaven;
		
	}

    /**
     * action stijltest
     *
     * @return void
     */
    public function stijltestAction(): ResponseInterface
    {
		$bedieningsprofielreg= (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens lezen,Netwerkgegevens schrijven");
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'stijltestAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$bedieningsprofiel=$this->args["bedieningsprofiel"];
		$personid=$bedieningsprofiel['person_uid'];
		$return=$this->stack[count($this->stack)-1]['action'];
//		$this->stack[]=array("action"=>"stijltest","personid"=>$personid,"bedieningsprofiel"=>$bedieningsprofiel);
		$this->stack[]=array("action"=>"stijltest","personid"=>$personid,"bedieningsprofiel"=>$bedieningsprofiel);
		$aStijltest=$this->BedieningsprofielRepository->findStijltest();
		$assignedValues = [
			'stijltest' => $aStijltest,
			'stack' => json_encode($this->stack),
			'userid' => $this->userid,
			'return' => $return,
			'personid' => $personid,
        ];
        $this->view->assignMultiple($assignedValues); 
    	return $this->responseFactory
        ->createResponse()
        ->withHeader('Cache-Control', 'no-cache')
        ->withHeader('Content-Type', 'text/html; charset=utf-8')
        ->withStatus(200, 'Super ok!')
        ->withBody($this->streamFactory->createStream($this->view->render()));

	}
	
		
    /**
     * action stijlberekenen
     *
     * @return aStijlen
     */
    public function stijlberekenen($personid)
    {
		// calculate stijltest score:
		$astijlscore=array(0,0);
		for ($i=1; $i<=14; $i++) 
		{	
			$score=$this->args['tx_churchgiftsreg_bedieningsprofiel']["a".$i];
			if ($i<8) $astijlscore[0]+=$score;else $astijlscore[1]+=$score;
		}
		if ($astijlscore[0]>21) $stijlorganisatie="Gestructureerd";else $stijlorganisatie="Ongestructureerd";
		if ($astijlscore[1]>21) $stijlenergie="Mensgericht";else $stijlenergie="Taakgericht";
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'stijlberekenenAction stijlorganisatie: '.urldecode(http_build_query($astijlscore,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		$aStijlen["stijlorganisatie"]=$stijlorganisatie;
		$aStijlen["stijlenergie"]=$stijlenergie;
		return $aStijlen;
	}


    /**
     * action save
     *
     * @return void
     */
    public function saveAction(): ResponseInterface
    {
		$bedieningsprofielreg= (int)churchpersreg_div::Heeftpermissie("Netwerkgegevens schrijven");
		$bedieningsprofiel = $this->args['bedieningsprofiel'];
		$personid=$bedieningsprofiel["person_uid"];

//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAction args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'saveAction bedieningsprofile: '.urldecode(http_build_query($bedieningsprofiel,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (!$bedieningsprofielreg && $personid && $personid!=$this->userid) die("You ($this->userid) don't have the privilege to perform this action on $personid");

		$bedieningsprofielid=$bedieningsprofiel['uid'];
	  	$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'SAVE PERSON args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 
		$key=array_search('edit', array_column($this->stack, 'action'));
		if ($key)
		{
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel bedieningsprofiel: '.str_replace('=','="',urldecode(http_build_query($bedieningsprofiel,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$commentrequired=0;
			$gesprekgewenst=0;
	        $ErrMsg=$this->BedieningsprofielRepository->save($bedieningsprofiel,$gesprekgewenst,$this->userid); 
			if (empty($ErrMsg))	array_splice($this->stack,$key);  // reset stack from this action off
		/*		{
			
				if (empty($bedieningsprofielid))
				{
					array_splice($this->stack,$key);  // new: reset stack from this action off
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel stack after reset from:'.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 
				}
				else 
					array_splice($this->stack,$key+1);  // update: reset stack after this action off
			if ($gesprekgewenst)	
				{	
//					$Curpath="https://".$_SERVER['SERVER_NAME'].str_replace("index.php","typo3conf/ext/churchpersreg/Classes/Hooks/EmailSend.php",$_SERVER["PHP_SELF"]);
					$Curpath="https://".$_SERVER['HTTP_HOST']."/index.php?eID=SendEmail";
					$aCfiles=array();  // array with curfiles
					$ch = curl_init($Curpath);
					curl_setopt($ch, CURLOPT_TIMEOUT, 1);
					curl_setopt($ch, CURLOPT_POST,1);
			                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
			                curl_setopt($ch, CURLOPT_PORT, 443);
					//$POST=\TYPO3\CMS\Core\Utility\GeneralUtility::_POST();
					$aoptions=array("subject"=>"Gesprek met gavenadviseur gewenst",
					"message"=>"Graag wil ik een gesprek met een gavenadviseur.",
					"recipients"=>'karelkuijpers@gmail.com<karel kuijpers>,ymi@ymago.nl<Ymi Dekker-Zijlstra>',
			 		"username"=>$this->frontendUser->user['name'],"useremail"=>$this->frontendUser->user['email'],"userid"=>$this->userid
	 				,"files"=>'',"archive"=>'',"confidential"=>'',"archivepersons"=>'');
//					$aoptions["recipients"]='karelkuijpers@gmail.com<karel kuijpers>';
					$aoptions["recipients"]='karelkuijpers@gmail.com<karel kuijpers>,ymi@ymago.nl<Ymkje Dekker-Zijlstra>';
					//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel curl options:'.str_replace('=','="',urldecode(http_build_query($aoptions,NULL,"=").'"')).'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 
					curl_setopt($ch, CURLOPT_POSTFIELDS , $aoptions);
					if (!$response=curl_exec($ch))$response=curl_error($ch);
					curl_close ($ch);
				} 
			}*/
			//else error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel ErrMsg:'.$ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 

		} 
		$assignedValues = [
			'stack' => json_encode($this->stack),
			'personid'=> $personid,
		];
        return (new ForwardResponse('detail'))
              ->withArguments($assignedValues);
    }


    /**
     * action delete
     *
     * @return void
     */
    public function deleteAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Netwerkgegevens schrijven"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'DELETE PERSON args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	  	$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel savecalled:'.$this->savecalled.'; begin stack: '.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 
		$key=count($this->stack)-1;
		$bedieningsprofielid=$this->args['bedieningsprofielid'];
		$adresid=$this->args['adresid'];
		$huisgenoten=$this->args['huisgenoten'];
		$action="selectedList";
	    $ErrMsg=$this->BedieningsprofielRepository->delete($bedieningsprofielid); 
//		error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'delete bedieningsprofiel REDIRECT to: selectedList; stack:'.str_replace('=','="',urldecode(http_build_query($this->stack,NULL,"=").'"'))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt'); 
		//$this->redirect($action,'Bedieningsprofiel','Churchgiftsreg',[ 'stack'=>json_encode($this->stack)]);
//		$this->forward("selectedList",null,null,['stack'=>json_encode($this->stack)]);
		$assignedValues = [
			'stack' => json_encode($this->stack),
		];
        return (new ForwardResponse('selectedList'))
              ->withArguments($assignedValues);
    }
	

	 /**
     * action export
     *
     * @return void
     */
    public function exportAction(): ResponseInterface
    {
		if (! churchpersreg_div::Heeftpermissie("Export bedieningsprofielen"))
		{	$this->ErrMsg='Geen rechten hiervoor'; return $this->ErrMsg;} 
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EXPORT bedieningsprofielen args: '.urldecode(http_build_query($this->args,NULL,"="))."\r\n");
	  	$this->stack=json_decode($this->args['stack'],true,20,JSON_INVALID_UTF8_IGNORE);
		$ErrMsg='';
		$key=array_search('selectedList', array_column($this->stack, 'action'));
		if (!$key===false)
		{
			$action='selectedList';
			$this->search=$this->stack[$key]['search'];
			array_splice($this->stack,$key+1);  // reset stack from this action off
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EXPORT search: '.urldecode(http_build_query($this->search,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
			$bedieningsprofielen=$this->BedieningsprofielRepository->findExportBedieningsprofielen($this->search);
			foreach ($bedieningsprofielen as $key=>$bedieningsprofiel)
			{
				if (!empty($bedieningsprofiel["gaven"])){$aGaven=explode(",",trim($bedieningsprofiel["gaven"]));}else{$aGaven=array();}
				$gaven=array();
			 	for ($i=0;$i<count($aGaven);$i++)
				{	
					$gaven[$i]=churchgiftsreg_div::$a_gaven[$aGaven[$i]];
				}
				$bedieningsprofielen[$key]["gaven"]=implode(', ',$gaven);
			}
			$exportfilenaam=churchpersreg_div::ExportToXlsx($bedieningsprofielen,'gifts','','',$this->frontendUser->user['name']);
		}
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'EXPORT stack nw: '.urldecode(http_build_query($this->stack,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
		if (empty($action)) $action='searchForm';

		$assignedValues = [
			'downloadfile' => "https://".$_SERVER['HTTP_HOST']."/".$exportfilenaam,
			'stack' => json_encode($this->stack),
			'return' => $action,
        ];
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'Export action:'.$action.'; assignedValues:'.urldecode(http_build_query($assignedValues,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3temp/churchadmin/log/debug.txt');
	//	$this->forward($action,null,null,$assignedValues);
        return (new ForwardResponse($action))
              ->withArguments($assignedValues);
	}
		
}
