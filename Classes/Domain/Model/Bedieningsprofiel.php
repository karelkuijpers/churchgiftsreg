<?php
namespace Parousia\Churchgiftsreg\Domain\Model;

/***
 *
 * This file is part of the "Sermons" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Karel Kuijpers <karelkuijpers@gmail.com>, Parousia Zoetermeer
 *
 ***/

/**
 * A speaker is a person who does the preaching
 */
class Bedieningsprofiel extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * tstamp
     *
     * @var DateTime
     */
    protected $tstamp = null;

    /**
     * personid
     *
     * @var int
     */
    protected $bedieningsprofielid = '';


    /**
     * naam
     *
     * @var string
     */
    protected $naam = '';


    /**
     * deleted
     *
     * @var bool
     */

    protected $deleted = false;

    /**
     * hidden
     *
     * @var bool
     */
    protected $hidden = false;

    /**
     * starttime
     *
     * @var DateTime
     */
    protected $starttime = null;

    /**
     * endtime
     *
     * @var DateTime
     */
    protected $endtime = null;

    /**
     * Returns the naam
     *
     * @return string $naam
     */
    public function getNaam()
    {
        return $this->naam;
    }
    /**
     * Sets the naam
     *
     * @param string $naam
     * @return void
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;
    }


    /**
     * Returns the deleted
     *
     * @return bool $deleted
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Sets the deleted
     *
     * @param bool $deleted
     * @return void
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * Returns the boolean state of deleted
     *
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Returns the tstamp
     *
     * @return DateTime $tstamp
     */
    public function getTstamp()
    {
        return $this->tstamp;
    }

    /**
     * Sets the tstamp
     *
     * @param DateTime $tstamp
     * @return void
     */
    public function setTstamp(\DateTime $tstamp)
    {
        $this->tstamp = $tstamp;
    }

    /**
     * Returns the hidden
     *
     * @return bool $hidden
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Sets the hidden
     *
     * @param bool $hidden
     * @return void
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * Returns the boolean state of hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * Returns the starttime
     *
     * @return DateTime $starttime
     */
    public function getStarttime()
    {
        return $this->starttime;
    }

    /**
     * Sets the starttime
     *
     * @param DateTime $starttime
     * @return void
     */
    public function setStarttime(\DateTime $starttime)
    {
        $this->starttime = $starttime;
    }

    /**
     * Returns the endtime
     *
     * @return DateTime $endtime
     */
    public function getEndtime()
    {
        return $this->endtime;
    }

    /**
     * Sets the endtime
     *
     * @param DateTime $endtime
     * @return void
     */
    public function setEndtime(\DateTime $endtime)
    {
        $this->endtime = $endtime;
    }
}
