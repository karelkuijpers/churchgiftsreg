<?php
namespace Parousia\Churchgiftsreg\Domain\Repository;
use Parousia\Churchlogin\Hooks\churchlogin_div;
use Parousia\Churchpersreg\Hooks\churchpersreg_div;
use Parousia\Churchgiftsreg\Hooks\churchgiftsreg_div;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;


/**
 * Class BedieningsprofielRepository
 *
 * @package Parousia\Churchpersreg\Domain\Repository
 *
 * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
 */
class BedieningsprofielRepository extends Repository
{
	var $ErrMsg=""; //error message
	var $bedieningsprofielReg=false;
	var $cleanteamReg=false;
	var $query;
	var $db;
	protected $aExport;

	
	public function findSelection(array $search = null)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);

		$statement=$this->GetBedieningsprofielen($search); 
	//	var_dump($statement);
	//	error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findSelection: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		$query->statement($statement);
		
 		try {
		      $bedieningsprofielen= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $bedieningsprofielen;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }

	public function findExportBedieningsprofielen(array $search = null)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);

		$statement=$this->GetExportBedieningsprofielen($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findeportSelection: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		$query->statement($statement);
		
 		try {
		      $bedieningsprofielen= $query->execute(true);
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		}
		
		try {
		      return $bedieningsprofielen;
		} catch(Exception $e) {
		 	echo 'Caught exception: ',  $e->getMessage(), "\n";
		} 
    }
	 /**
     * Override default findByUid function 
     *
     * param int  $pid                 id of person
     *
     * return bedieningsprofiel
     */
    public function findByUid($pid = NULL)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
        $statement=$this->GetBedieningsprofiel($pid);
//		var_dump($statement);
		$query->statement($statement);
        $results= $query->execute(true);
//		var_dump($results[0]);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid result[0]:'.urldecode(http_build_query($results,NULL,"="))."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');


		$bedieningsprofiel=$results[0];
		//rror_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid opm:'.GeneralUtility::array2xml($bedieningsprofiel)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
				
		return $bedieningsprofiel;
	}

	/**
     * function findGaventest function 
     *
     * return gaventest
     */
    public function findGaventest()
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
        $statement="SELECT uid,question,destination from gaventest";
//		var_dump($statement);
		$query->statement($statement);
        $results= $query->execute(true);
//		var_dump($results[0]);
		$gaventest=$results;
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid opm:'.GeneralUtility::array2xml($bedieningsprofiel)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
				
		return $gaventest;
	}

	/**
     * function findStijltest function 
     *
     * return stijltest
     */
    public function findStijltest()
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
        $statement="SELECT uid,question,stijlleft,stijlright,type from stijltest";
//		var_dump($statement);
		$query->statement($statement);
        $results= $query->execute(true);
//		var_dump($results[0]);
		$stijltest=$results;
//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'findByUid opm:'.GeneralUtility::array2xml($bedieningsprofiel)."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
				
		return $stijltest;
	}

	/**
	 * Method 'Save' for the 'churchgiftsreg' extension.
 	*
	 * param bedieningsprofiel required 
	 * returns true or error message
	 */
	
	public function save(&$bedieningsprofiel,&$gesprekgewenst,$userid)
	{
		$query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setIgnoreEnableFields(FALSE);
		$this->ErrMsg="";
		$bedieningsprofiel_id=intval($bedieningsprofiel['uid']);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'save bedieningsprofiel bedieningsprofiel_id: '.$bedieningsprofiel_id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		
		$gesprekgewenst=0;
		if (isset($bedieningsprofiel['gesprekgewenst']) and $bedieningsprofiel['gesprekgewenst']!=$bedieningsprofiel['gesprekgewenstorg'])$gesprekgewenst=$bedieningsprofiel['gesprekgewenst'];
			
		// initialize mysqli:
		churchpersreg_div::connectdb($this->db);
		$fields='verlangen=AES_ENCRYPT("'.$this->db->escape_string($bedieningsprofiel['verlangen']).'",@password),'.
		'gaven=AES_ENCRYPT("'.((isset($bedieningsprofiel['gaven']))?implode(",",$bedieningsprofiel['gaven']):'').'",@password),'.
		'stijlenergie="'.$bedieningsprofiel['stijlenergie'].'",stijlorganisatie="'.$bedieningsprofiel['stijlorganisatie'].'",'.
		'leuk_in_werk=AES_ENCRYPT("'.$this->db->escape_string($bedieningsprofiel['leuk_in_werk']).'",@password),'.
		'ervaring=AES_ENCRYPT("'.$this->db->escape_string($bedieningsprofiel['ervaring']).'",@password),'.
		'wat_aanspreekt=AES_ENCRYPT("'.((is_array($bedieningsprofiel['wat_aanspreekt']))?implode(",",$bedieningsprofiel['wat_aanspreekt']):'').'",@password),'.
		'overig=AES_ENCRYPT("'.$this->db->escape_string($bedieningsprofiel['overig']).'",@password),'.
		'vaardigheden=AES_ENCRYPT("'.((isset($bedieningsprofiel['vaardigheden']))?implode(",",$bedieningsprofiel['vaardigheden']):'').'",@password),'.
		'anders=AES_ENCRYPT("'.$this->db->escape_string($bedieningsprofiel['anders']).'",@password),'.
		'person_id=AES_ENCRYPT("'.$bedieningsprofiel['person_uid'].'",@password),'.
		((empty($gesprekgewenst))?'datum_lst_verzoekgesprek="0000-00-00",':'datum_lst_verzoekgesprek=curdate(),').
		'steller="'.$userid.'"';

		
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'bedieningsprofiel fields: '.$fields."; bedieningsprofiel_id:".$bedieningsprofiel_id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		if (empty($bedieningsprofiel_id))
		{	
			// check if bedieningsprofiel has already been entered:
			$statement='insert bedieningsprofiel set '.$fields;
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'insert bedieningsprofiel: '.$statement."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
			if ($this->db->query($statement))
			{
				$bedieningsprofiel_id=$this->db->insert_id;
				$bedieningsprofiel['uid']=$bedieningsprofiel_id;
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'inserted bedieningsprofiel id: '.$bedieningsprofiel_id."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
			}
			else 
			{
				$this->ErrMsg="insert bedieningsprofiel wrong:".$this->db->error;
				//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'inserted bedieningsprofiel fout: '.$this->ErrMsg."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
			} 
		}
		else
		{
			$statement='update bedieningsprofiel set '.$fields.' where uid="'.$bedieningsprofiel_id.'"';
			$results=$this->db->query($statement);
			//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'update bedieningsprofiel: '.$statement.'; error:'.$this->db->error.", affected:".$this->db->affected_rows."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		}
		$numberChanged=0;
		if (empty($this->ErrMsg))
		{
			$numberChanged=$this->db->affected_rows;
			// save beroep in persoon:
			$this->db->query('update persoon set beroep="'.$this->db->escape_string($bedieningsprofiel['beroep']).'" where uid="'.intval($bedieningsprofiel['person_uid']).'"');
			if ($this->db->affected_rows>0)$numberChanged=1;
		}
		return $this->ErrMsg;
	}
	
	/**
	 * Method 'GetBedieningsprofielen' for the 'churchgiftsreg' extension.
 	*
	 * param searchfield: keyword required bedieningsprofiels
	 * returns string with select statement 
	 */
	function GetBedieningsprofielen(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//	$this->ErrMsg="Getbedieningsprofiels zoekveld 1:".$Zoekveld_1.",2:".$Zoekveld_2.",3:".$Zoekveld_3.",4:".$Zoekveld_4.",5:".$Zoekveld_5.",6:".$Zoekveld_6;
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetBedieningsprofielen:bezoeker: '.$Bezoeker."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');

		$zoekargument=$this->ComposeSearch($search);
		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetBedieningsprofielen zoekargument:'.$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		$query=' adres as ta,';
		if ($search['general']<>"")
		{	$query .=' persoon as tp left join (taakbekleding as tb, taak as tk) on (tb.id_persoon=tp.uid and tb.id_parent=tk.uid), bedieningsprofiel as pr LEFT JOIN persoon as tad ON tad.uid=adviseur_id  ';
			// toevoegen voor taak en taakbekleding:
			$query.=' where tp.uid=AES_DECRYPT(person_id,@password) and pr.deleted=0 and ';		
		}
		elseif ($search['naw']<>"")
		{	$query .=' persoon as tp LEFT JOIN bedieningsprofiel as pr ON (tp.uid=AES_DECRYPT(person_id,@password) and pr.deleted=0) where ';}
		else {$query .=' persoon as tp, bedieningsprofiel as pr where tp.uid=AES_DECRYPT(person_id,@password) and pr.deleted=0 and ';}
		$query.=$zoekargument; 

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetBedieningsprofielen: zoekargument:'.$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');

		$statement = 'SELECT DISTINCT tp.uid as person_uid'.
		', concat(tp.achternaam,", ",tp.tussenvoegsel," ",tp.roepnaam) as naam'.
		', lower(AES_DECRYPT(pr.verlangen,@password)) as verlangen'.
		', lower(AES_DECRYPT(pr.gaven,@password)) as gaven'.
		' FROM '.$query.' ORDER BY tp.achternaam';
		return ($statement);
	}
	

	/**
	 * Method 'GetExportBedieningsprofielen' for the 'churchgiftsreg' extension.
 	*
	 * param searchfield: keyword required bedieningsprofiels
	 * returns string with select statement 
	 */
	function GetExportBedieningsprofielen(array $search = null)
	{	
	//	Samenstellen van zoekargument

	$zoekargument=$this->ComposeSearch($search);
		$query=' adres as ta,';
		if ($search['general']<>"")
		{	$query .=' persoon as tp left join (taakbekleding as tb, taak as tk) on (tb.id_persoon=tp.uid and tb.id_parent=tk.uid), bedieningsprofiel as pr LEFT JOIN persoon as tad ON tad.uid=adviseur_id  ';
			// toevoegen voor taak en taakbekleding:
			$query.=' where tp.uid=AES_DECRYPT(person_id,@password) and pr.deleted=0 and ';		
		}
		elseif ($search['naw']<>"")
		{	$query .=' persoon as tp LEFT JOIN bedieningsprofiel as pr ON (tp.uid=AES_DECRYPT(person_id,@password) and pr.deleted=0) where ';}
		else {$query .=' persoon as tp, bedieningsprofiel as pr where tp.uid=AES_DECRYPT(person_id,@password) and pr.deleted=0 and ';}
		$query.=$zoekargument; 

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'GetBedieningsprofielen:bezoeker: '.$Bezoeker."; zoekargument:".$zoekargument."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');
		
		$statement = 'SELECT DISTINCT '.
		'concat(tp.achternaam,", ",tp.tussenvoegsel," ",tp.roepnaam) as naam'.
		', straatnaam,huisnummer,postcode,woonplaats'.
		', lower(AES_DECRYPT(pr.verlangen,@password)) as verlangens'.
		', lower(AES_DECRYPT(pr.gaven,@password)) as gaven'.
		', tp.lid'.
		' FROM '.$query.' ORDER BY tp.achternaam';
		return ($statement);
	}

		/**
	 * Method 'ComposeSearch' for the 'churchgiftsreg' extension.
 	*
	 * param searchfield: keyword required bedieningsprofiel 
	 * returns composed sql search parameter
	 */
	function ComposeSearch(array $search = null)
	{	
	//	Samenstellen van zoekargument
	//			$this->ErrMsg.="Compose zoekveld 1:".$Zoekveld[1].",2:".$Zoekveld[2].",3:".$Zoekveld[3].",4:".$Zoekveld[4].",5:".$Zoekveld[5].",6:".$Zoekveld[6].",7:".$Zoekveld[7];
		$zoekargument="";
		$Not=false;
		$connection=GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)->getConnectionForTable('string');
	/*	if (!empty($Zoekveld[7])){
			$zoekargument .=  "(tp.uid = '" . addslashes($Zoekveld[7]) ."') AND ";}		*/
		if ($search['verlangen']<>""){ 
			$search['verlangen']=trim($search['verlangen']);
			$zoekargument .=  "lower(AES_DECRYPT(pr.verlangen,@password)) LIKE '%" . $search['verlangen']."%' AND ";
		}
		if ($search['gaven']<>""){ 
			foreach ($search['gaven'] as $mygave){
				$zoekargument .=  "lower(AES_DECRYPT(pr.gaven,@password)) LIKE '%" . $mygave ."%' AND ";
			}
		}
		if ($search['vaardigheden']<>""){ 
			foreach ($search['vaardigheden'] as $myvaardig){
				$zoekargument .=  "lower(AES_DECRYPT(pr.vaardigheden,@password)) LIKE '%" . $myvaardig ."%' AND ";
			}
		}
		if (!empty($search['islid']))
		{
			$zoekargument .= "tp.lid='is lid' AND ";
		}
/*		if (!empty($search['gesprekgewenst']))
		{
			$zoekargument .= 'pr.datum_lst_verzoekgesprek>pr.datum_lst_gesprek and pr.datediff(curdate(),pr.datum_lst_verzoekgesprek)<300 AND ';
		} */
		if (!empty($search['naw'])){ 
			$a_key=$this->DeriveKeywords($search['naw']);
			foreach ($a_key as $key)
			{	$key=trim($key);
				if (!empty($key))
				{	// key:
					if (substr($key,0,1)=="-")
					{	
						$zoekargument.="NOT ";  // ontkenning
						$key=substr($key,1);
						$Not=true;
					}			
					$key=addslashes($key);
						//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekwoord:".$key,"churchgiftsreg");
					$zoekargument.=' (concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) like "%'.$key.'%" OR ';
					// ook op andere velden zoeken:
					$zoekargument .=  "tp.voornamen LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "straatnaam LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "huisnummer LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "postcode LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "woonplaats LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "land LIKE '%" . $key ."%'  ) and ";	
				}
			}
		}
		if ($search['general']<>""){ 
			$a_key=$this->DeriveKeywords($search['general']);
			foreach ($a_key as $key)
			{	$key=trim($key);
				if (!empty($key))
				{	// key:
					if (substr($key,0,1)=="-")
					{	
						$zoekargument.="NOT ";  // ontkenning
						$key=substr($key,1);
						$Not=true;
					}			
					$key=addslashes($key);
						//TYPO3\CMS\Core\Utility\GeneralUtility::devLog("zoekwoord:".$key,"churchgiftsreg");
/*					$zoekargument.=' (concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) like "%'.$key.'%" OR ';
					// ook op andere velden zoeken:
					$zoekargument .=  "tp.voornamen LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "straatnaam LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "huisnummer LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "postcode LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "woonplaats LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "land LIKE '%" . $key ."%' OR "; */
					$zoekargument .=  '(pr.stijlenergie LIKE "' . $key .'%" OR ';
					$zoekargument .=  'pr.stijlorganisatie LIKE "' . $key .'%" OR ';
					$zoekargument .=  "lower(AES_DECRYPT(pr.verlangen,@password)) LIKE '%" . $key ."%' OR ";
					// gaven: bepaal bijbehorende indexes:
					$zoekargument.= $this->getzoekkey($key, churchgiftsreg_div::$a_gaven,"pr.gaven");
					$zoekargument .=  "lower(AES_DECRYPT(pr.anders,@password)) LIKE '%" . $key ."%' OR ";
					// wat aanspreekt: bepaal bijbehorende indexes:
					$zoekargument.= $this->getzoekkey($key, churchgiftsreg_div::$a_Aanspreek,"wat_aanspreekt");
					// vaardigheden: bepaal bijbehorende indexes:
					$zoekargument.= $this->getzoekkey($key, churchgiftsreg_div::$a_vaardigheid,"pr.vaardigheden");
					$zoekargument .=  "lower(AES_DECRYPT(pr.overig,@password)) LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "lower(AES_DECRYPT(pr.inzet,@password)) LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "lower(AES_DECRYPT(leuk_in_werk,@password)) LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "lower(voorgestelde_bedieningen) LIKE '%" . $key ."%' OR ";
//					$zoekargument .=  "lower(redenweigering) LIKE '%" . $key ."%' OR ";
					$zoekargument .=  "tp.beroep LIKE '%" . $key ."%' OR ";
					// huidige taak: bepaal bijbehorende indexen:
					$zoekargument .= "tk.omschrijving LIKE '%".$key."%' ) and ";				
				}
			}
		}
		$zoekargument.=' AES_DECRYPT(tp.id_adres,@password)=ta.uid and tp.deleted=0 and ta.deleted=0';
//		$this->ErrMsg.=",br>Zoekargument:".$zoekargument;
		return $zoekargument;
	}
	
	/*
	 * function DeriveKeywords:
	 * Derive search keywords from a Google-like searchfield $strZoek
	 */
	function DeriveKeywords($strZoek){
		// bepalen van zoekwoorden uit een vrije tekst veld
		$aZoekwoord=array();
		$aZoekveld=array();
		$Quote="";
		$WordQ="";
		$Min="";
		// Replace &quote; back to ":
		$strZoek=str_ireplace("&quot;", '"', $strZoek);
		// splits in losse woorden:
		$aZoekwoord = preg_split ("/[\s,;]+/", trim($strZoek)); 
		// voeg strings tussen quote's weer samen:
		for ($i=0;$i<count($aZoekwoord);++$i)
		{	$Woord=$aZoekwoord[$i];
			if (empty($Quote))
			{	// zoeken naar quote:
				// eerst checken op +-
				if (substr($Woord,0,1)=="-")
				{	$Min="-";
					$Woord=substr($Woord,1);
				}
				else
				{	$Min="";
					if (substr($Woord,0,1)=="+"){$Woord=substr($Woord,1);} //+ schrappen
				}
					
				if (substr($Woord,0,1)=='"' or substr($Woord,0,1)=="'")
				{	$Quote=substr($Woord,0,1);
					$aZoekwoord[$i]=$Min.substr($Woord,1);
					$Min="";
					--$i; // zoekwoord nogmaals meenemen voor verwerking
				}
				else
				{	array_push($aZoekveld,addslashes($Min.$Woord));
				}
			}
			else
			{	// zoeken naar eind quote:
				if (substr($Woord,strlen($Woord)-1,1)==$Quote)
				{	$WordQ.=" ".substr($Woord,0,strlen($Woord)-1);
					array_push($aZoekveld,trim(addslashes($WordQ)));
					$WordQ="";
					$Quote="";
				}
				else
				{	$WordQ.=" ".$Woord;
				}
			}
		}
		if (!empty($WordQ)){array_push($aZoekveld,$WordQ);} // laatste tot eind ook meenemen
		return $aZoekveld;
	}
	
	function getzoekkey($searchvalue, $a_options,$dbname)
	/*	Bepaal keys uit array $a_options, waarvan de value overeenkomt met $searchvalue
		Geeft terug SQL Select zoekargument voor $dbname
	*/
	{
		$searchargument="";
		foreach ($a_options as $optkey => $group)
		{	
			IF (IS_ARRAY($group))
			{
				if (strcasecmp($optkey,$searchvalue)==0) // hele groep voldoet aan searchvalue:
				{
					foreach ($group as $elemkey => $elem)
						{$searchargument .=  "AES_DECRYPT($dbname,@password) LIKE '%" . $elemkey ."%' OR ";}
				}
				else
				{
					foreach ($group as $elemkey => $elem)
					{
						if (stristr($elem,$searchvalue)!==false) // key gevonden?
						{
							$searchargument .=  "AES_DECRYPT($dbname,@password) LIKE '%" . $elemkey ."%' OR ";
						}
					}
				}
			}
			else
			{
				if (stristr($group,$searchvalue)!==false) // key gevonden?
				{
					$searchargument .=  "AES_DECRYPT($dbname,@password) LIKE '%" . $optkey ."%' OR ";
				}
			}
		}
		if (strlen($searchargument)>0) // iets gevonden?
			{$searchargument="(".substr($searchargument,0,strlen($searchargument)-3).") OR ";}  // verwijder laatste OR
	//		exit($searchargument);
		return $searchargument;
	}

	
	/**
	 * Method 'GetBedieningsprofiel' for the 'churchgiftsreg' extension.
 	*
	 * param bedieningsprofiel_id: personid of selected bedieningsprofiel
	 * returns statement to find bedieningsprofiel 
	 */
	function GetBedieningsprofiel($personid)
	{	
		$statement = 'SELECT DISTINCT tp.uid as person_uid'.
		', concat(tp.roepnaam," ",tp.tussenvoegsel," ",tp.achternaam) as naam'.
		', opm.uid,AES_DECRYPT(verlangen,@password) as verlangen'.
		', AES_DECRYPT(gaven,@password) as gaven'.
		', AES_DECRYPT(vaardigheden,@password) as vaardigheden'.
		', AES_DECRYPT(overig,@password) as overig'.
		', AES_DECRYPT(wat_aanspreekt,@password) wat_aanspreekt'.
		', AES_DECRYPT(anders,@password) as anders'.
		', stijlenergie, stijlorganisatie'.
		', datum_lst_verzoekgesprek'.
		', if(datum_lst_verzoekgesprek>datum_lst_gesprek and datediff(curdate(),datum_lst_verzoekgesprek)<300,1,0) as gesprekgewenst'.
		', beroep'.
		', AES_DECRYPT(leuk_in_werk,@password) as leuk_in_werk'.
		', AES_DECRYPT(person_id,@password) as person_id'.
		', AES_DECRYPT(ervaring,@password) ervaring'.
		', AES_DECRYPT(inzet,@password) as inzet'.
		', UNIX_TIMESTAMP(opm.datum_wijziging) as datum_wijziging'.
		', concat(coalesce(tkn.taken,""),if(tkn.taken is null,"",if(ldr.leider is null,"",", ")),coalesce(ldr.leider,"")) as taken'. 
		' FROM (select group_concat(concat("leider ",omschrijving) separator ", ") as leider from bediening b where find_in_set("'.$personid.'",id_bedieningsleider) ) as ldr,'.
		' persoon as tp left join bedieningsprofiel as opm on(AES_DECRYPT(opm.person_id,@password)=tp.uid)'.
		' left join (select group_concat(tk.omschrijving order by tk.omschrijving asc separator ", ") as taken,med.id_persoon from taakbekleding med left join taak tk on (tk.uid = med.id_parent) where med.id_persoon="'.$personid.'") as tkn on (tkn.id_persoon=tp.uid) '.
		' where tp.uid="'.$personid.'"';

		//error_log(date("Y-m-d H:i:s")." - ".$_SERVER['PHP_SELF'].": ".'one getbedieningsprofiel: '.$statement ."\r\n",3,$_SERVER['DOCUMENT_ROOT'].'/typo3conf/ext/churchgiftsreg/Classes/Controller/debug.txt');

		return $statement;
	}
	


	function cmp ($a, $b) { 
	   return strcmp($a[0], $b[0]); 
	} 


	
}