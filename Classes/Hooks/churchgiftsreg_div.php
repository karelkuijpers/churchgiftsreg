<?php
namespace Parousia\Churchgiftsreg\Hooks;
/***************************************************************
*  Copyright notice
*
*  (c) 1999-2008 Karel Kuiojpers (karelkuijpers@gmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/* *
 * @author	Karel Kuijpers <karelkuijpers@gmail.com>
 * @package TYPO3
 * @subpackage churchgiftsreg
 */
class churchgiftsreg_div {
	// variabelen:
	// OPM tabellen:
	static public $a_gaven=array(
		'a' => "apostelschap",
		'b' => "barmhartigheid",
		'c' => "bemoediging",
		'd' => "bestuur",
		'e' => "creatieve communicatie",
		'f' => "dienen",
		'g' => "evangelisatie",
		'h' => "gastvrijheid",
		'i' => "geloof",
		'j' => "geven",
		'k' => "herderschap",
		'l' => "kennis",
		'm' => "leiderschap",
		'n' => "onderscheid",
		'o' => "onderwijs",
		'p' => "profetie",
		'q' => "vakmanschap",
		'r' => "voorbede",
		's' => "wijsheid"
		);
	
	static public $a_vaardigheid=array(
		"Professioneel" => array(
		101=>"psychiatrie",
		102 => "sociaal werk",
		103 => "geneeskunde",
		104 => "fysiotherapie",
		105 => "rechten",
		106 => "boekhouding",
		107 => "belastingen",
		108 => "public relations",
		109 => "computerprogrammeur",
		110 => "systeemanalist",
		111 => "journalistiek",
		117 => "muzikant"),
		"Kunst" => array(
		115 => "fotografie",
		116 => "handvaardigheid",
		113 => "tekenen/schilderen",
		114 => "beeldhouwen"),
		"Onderwijs aan" => array(
		121 => "peuter en kleuters",
		122 => "basisschool",
		123 => "middelbare school",
		124 => "hoger onderwijs"),
		"Techniek" => array(
		131 => "onderhoud aan auto's",
		132 => "onderhoud aan bromfietsen",
		133 => "onderhoud aan fietsen",
		134 => "reparatie huishd.apparatuur",
		135 => "webdesign",
		136 => "filmpjes maken",
		137 => "geluid",
		138 => "decorbouw",
		139 => "decorontwerp",
		140 => "licht"),
		"Kantoorwerk"=>array(
		151 => "tekstverwerking",
		152 => "administratief werk",
		153 => "receptie",
		154 => "magazijn",
		155 => "secretariaat"),
		"Bouw"=>array(
		161 => "aannemer",
		162 => "architect",
		163 => "timmerman",
		164 => "meubelmaker",
		165 => "elektricien",
		166 => "loodgieter",
		167 => "verwarmingstechniek",
		168 => "airconditioning",
		169 => "schilderen",
		170 => "behangen",
		171 => "metselen",
		172 => "dakwerken",
		173 => "lokaal netwerk",
		174 => "tapijtleggen",
		175 => "bouwkundig tekenaar"),
		"Werken met"=>array(
		181 => "gehandicapten",
		182 => "doven",
		126 => "jong volwassenen (18-29)",
		125 => "alleengaanden (30+)",
		183 => "kinderen met leer/opv.probl.",
		186 => "peuters en kleuters",
		185 => "kinderen (7-12 jr)",
		184 => "tieners (13-16 jr)",
		185 => "babies",
		127 => "echtparen"),
		"Algemeen"=>array(
		201 => "kinderopvang",
		202 => "tuinieren",
		203 => "onderhoud gebouwen",
		204 => "vervoer",
		205 => "koken voor grote groepen",
		206 => "boekenwinkel",
		207 => "schoonmaken",
		208 => "sport")
		);
	
	static public $a_Aanspreek=array(
		10 => "zingen",
		11 => "zangleiding",
		12 => "muziek maken",
		13 => "mensen praktisch helpen",
		14 => "mensen geestelijk(verder)helpen",
		15 => "bijbelstudie geven",
		16 => "prediken",
		17 => "organiseren",
		18 => "besturen/coördineren",
		19 => "leiding geven (algemeen)",
		20 => "een kring leiden",
		21 => "crêche- of peuterwerk",
		22 => "kinderwerk",
		23 => "tienerwerk",
		24 => "jeugd- of jongerenwerk",
		25 => "werken met ouderen",
		26 => "mensen bezoeken",
		27=> "mensen thuis ontvangen",
		28 => "met computers werken",
		29 => "technische problemen oplossen",
		30 => "financiële zaken",
		31 => "evangelisatiewerk",
		32 => "artikelen schrijven,interviewen",
		33 => "redactiewerk(redigeren,vormgv.)",
		34 => "zendingswerk",
		35 => "iets voor de zending doen",
		36 => "gebedsactiviteiten(alleen/groep)",
		37 => "creatieve werkvormen(drama,dans)");
		
	static function OptgroupList($selectvalues, $inhoudArry,$withkey = false)
	{
		$sel="";
		$optgrp="";
		if (!is_array($selectvalues)){$selectvalues=array($selectvalues);}
		//Controleren of meegegeven array daadwerkelijk een array is.
		if (Is_Array($inhoudArry)==false) 
			{$opties= "&nbsp;";}
		else
		{
			// Doorloop alle waardes.
			foreach ($inhoudArry as $optkey => $group)
			{
				IF (IS_ARRAY($group)) // option group?
				{	$opties='<OPTGROUP label="'.$optkey.'">';
					foreach ($group as $elemkey => $elem)
					{
						// Controleren of dit element een geselecteerde element is.
						$sel="";
						if ($withkey)
						{	
							if (array_search($elemkey,$selectvalues)!==false)
								{$sel="SELECTED";}
							$sel.=" value=$elemkey";
						}
						else
						{
							if (array_search($elem,$selectvalues)!==false)
							{$sel="SELECTED";}
						}
						// Bouw de optie.
						$elem=trim($elem);
						$opties.="<Option $sel>$elem</OPTION>";
					}
					$optgrp.=$opties."</OPTGROUP>";
				}
				else
				{
					// Controleren of dit element een geselecteerde element is.
					$sel="";
					if ($withkey)
					{	
						if (array_search($optkey,$selectvalues)!==false)
							{$sel="SELECTED";}
						$sel.=" value=$optkey";
					}
					else
					{
						if (array_search($group,$selectvalues)!==false)
						{$sel="SELECTED";}
					}
					// Bouw de optie.
					$optgrp.="<Option $sel>$group</OPTION>";				
				}	
				
			}
		}
		return $optgrp;
	}
}
